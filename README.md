# Quay Deployment Role

This role will deploy:

- Quay operator by applying quay-operator.yml
  * 1 CRD, 1 Deployment, 1 Pod, 1 Replica set
  
- Quay Instance by applying quay-enterprise-https.yml
  * 5 Deployments, 5 Pods, 1 PVC, 4 Services, 2 Routes
  
  Components are PostgreSQL that will be run with Persistant Volume of 10G (Configurable), Redis, Quayecosystem, Quayrepomirror, 
  Quayconfig that will be removed after configuration so at the end config pod and config route will be removed automatically
  
Role that can be run also independently from another template if needed

 - role-quay-enterprise

Role includes
- main.yml under tasks folder
- quay-operator.yml under templates folder
- quay-enterprise-https.yml under templates folder

## Assumptions and special notes

* This role can run only after you can login to your cluster by htpasswd user
* This role should run after playbook that will define what Storage class is default (Trident, for example)
* This role should run after role that will create Object Storage, Minio in our case 
* This role will always configure external access to quay with https. In case there are no custom certificates it will use ingress defaul certificates
* This role configures connection betweem quay and minio (which is the backend) INSIDE the cluster. See architecture
section
* Sometimes you can encounter with issue that Quay PostgreSQL pod stacked and only in this case you need to add in the PostgreSQL Deployment yaml file 
  under security context parameter: fsGroup: 26 , you can find this commented value in the quay-enterprise.yml 
  (I tried to include this anyway, but usually it didn't need and it cause to postgreSQL PVC not to be created, that's why I commented it)

## add to you inventory the following parameters:
--------------------------------------------------------------------------------
* **#Quay deployment**
* **quayproject:** quay-enterprise
* **quayoperatorimage:** quay.io/redhat-cop/quay-operator:v1.1.2
* **quayecosystem:** prod-quayecosystem
* **quayimage:** quay.io/projectquay/quay:qui-gon
* **quaydbimage:** registry.access.redhat.com/rhscl/postgresql-96-rhel7:1
* **quaydbsize:** 10Gi
* **quayredisimage:** registry.access.redhat.com/rhscl/redis-32-rhel7:latest
* **quaydbusername:** quaydb
* **quaypassword:** Password@123
* **quaydbrootpass:** Mamram123!@#
* **quaydbname:** quaydb
* **quayusername:** cloudlet_admin
* **quayemail:** quay@cloudlet.com
* **quayhostname:** quay
* **quayconfig:** quayconfig

## Architecture
![images](connection_architecture.png)

Quay (as minio) has edge route that configures tls connection between the clients and the ingress, which is forwarded to the service by the ingress in unsecure way (tls is implemented only on the outside). 

Quay also needs connection to MinIO, as this is its backend, and we wanted it to be secured. At first we tried to connect them between routes, meaning that a packet will get out of the cluster through quay route and then back again into the minio route and through the ingress. This is clumsy and unnecessary as openshift allow services inside cluster to communicate. So now quay interacts with minio through its service + port 9000.
